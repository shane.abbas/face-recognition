Uses this library: https://github.com/ageitgey/face_recognition. It is a python library, so you should have python 3 and pip.

Installation Instructions can be found here: https://github.com/ageitgey/face_recognition#installation-options

For windows, this is what worked:
- Download Visual Studio https://www.visualstudio.com/thank-you-downloading-visual-studio/?sku=Community&rel=15
    - Just select Desktop development with C++
- Download CMake https://cmake.org/files/v3.11/cmake-3.11.3-win64-x64.msi
- pip install dlib
- pip install face_recognition

To train the model, create a folder like so: profile_images/person_name/image_name