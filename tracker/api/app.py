import os
import MySQLdb
from flask import Flask, jsonify, request, render_template
import json


app = Flask(__name__)


@app.route("/heartbeat")
def heartbeat():
    return "OK", 200


@app.route('/add/<int:camera_id>', methods=["POST", "GET"])
def add(camera_id):
    if "person_name" not in request.args:
        return "Missing person_name in request arg", 503
    db = db_connect()
    cur = db.cursor()
    query = """
        INSERT INTO tracked_location (person_name, camera_id, timestamp)
        VALUES (%s, %s, CURRENT_TIMESTAMP)
        """
    params = (request.args["person_name"], camera_id)

    cur.execute(query, params)
    db.commit()
    return "Added", 200


@app.route("/get", methods=["GET"])
@app.route("/get/<user_str>", methods=["GET"])
def get(user_str=None):
    db = db_connect()
    cur = db.cursor()
    if user_str is None:
        query = """
            SELECT t.id, t.person_name, t.camera_id, t.timestamp, c.name, c.location
            FROM tracked_location t INNER JOIN camera c ON t.camera_id = c.id
            WHERE t.timestamp = (
                SELECT MAX(t2.timestamp) FROM tracked_location t2 WHERE t2.person_name = t.person_name
            )
            """
        params = ()
    else:
        query = """
            SELECT t.id, t.person_name, t.camera_id, t.timestamp, c.name, c.location
            FROM tracked_location t INNER JOIN camera c ON t.camera_id = c.id
            WHERE t.person_name = %s ORDER BY t.timestamp DESC LIMIT 1
            """
        params = (user_str,)

    cur.execute(query, params)
    result = []
    for id, person_name, camera_id, timestamp, camera_name, camera_location in cur:
        result.append({
            "track_id": id,
            "person_name": person_name,
            "timestamp": timestamp,
            "camera_id": camera_id,
            "camera_name": camera_name,
            "camera_location": camera_location
        })
    return jsonify(result)


@app.route("/cleartrack", methods=["GET"])
def clear():
    db = db_connect()
    cur = db.cursor()
    query = """
        TRUNCATE TABLE tracked_location
        """
    cur.execute(query)
    return "OK", 200


@app.route("/", methods=["GET"])
def webview():
    return render_template('index.html', people=json.loads(get().get_data()))


def db_connect():
    db = MySQLdb.connect("db", "viper", "viper67", "tracker")
    return db


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=os.environ.get("PORT", 80))
