CREATE DATABASE IF NOT EXISTS `tracker`;
use tracker;

CREATE TABLE IF NOT EXISTS `camera` (
    `id` int(10) NOT NULL auto_increment,
    `name` varchar(255),
    `location` varchar(255),
    PRIMARY KEY( `id` )
);

CREATE TABLE IF NOT EXISTS `tracked_location` (
    `id` int(10) NOT NULL auto_increment,
    `person_name` varchar(128),
    `camera_id` int,
    `timestamp` datetime,
    PRIMARY KEY( `id` )
);

GRANT ALL PRIVILEGES ON tracker.* TO 'viper'@'%';

