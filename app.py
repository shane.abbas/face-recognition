from pprint import pprint
import tkinter
import cv2
import PIL.Image, PIL.ImageTk
import time
import face_recognition
import face_recognition.api
import os
import pyttsx3
import pickle
from cacheout import Cache
import requests
import json
import random


class App:
    def __init__(self, window, window_title, video_source=0):
        self.config = json.loads(open("config.json").read())
        # print(self.config)

        self.do_face_recognizer = False
        self.tts = pyttsx3.init()

        trained_data = None
        if os.path.isfile('company_data.pkl'):
            print("FaceRec file found")
            with open('company_data.pkl', 'rb') as inp:
                print("Loading FaceRec file")
                trained_data = pickle.load(inp)
        else:
            print("No FaceRec file")
            trained_data = self.load_training_images(self.config["images_path"])
            with open('company_data.pkl', 'wb') as output:
                pickle.dump(trained_data, output, pickle.HIGHEST_PROTOCOL)

        self.face_recog = FaceRecognizer(trained_data)

        self.window = window
        self.window.title(window_title)
        self.video_source = video_source

        self.greeting_cache = Cache(maxsize=self.config['greeting_cache']['maxsize'], ttl=self.config['greeting_cache']['ttl'])
        self.last_seen_cache = Cache(maxsize=self.config['last_seen_cache']['maxsize'], ttl=self.config['last_seen_cache']['ttl'])

        # open video source (by default this will try to open the computer webcam)
        self.vid = MyVideoCapture(self.video_source)

        # Create a canvas that can fit the above video source size
        self.canvas = tkinter.Canvas(window, width = self.vid.width, height = self.vid.height)
        self.canvas.grid(column=0, row=0)

        # Button that lets the user take a snapshot
        self.btn_snapshot=tkinter.Button(window, text="Snapshot", width=50, command=self.snapshot)
        self.btn_snapshot.grid(column=0, row=1)

        # Button that triggers face recognizatiobn
        self.btn_face_recognization=tkinter.Button(window, text="Face Recognizer", width=50, command=self.flip_face_recognizer)
        self.btn_face_recognization.grid(column=0, row=2)

        # Button to clear cache
        self.btn_clear_cache = tkinter.Button(window, text="Clear Cache", width=50, command=self.clear_cache)
        self.btn_clear_cache.grid(column=0, row=3)

        # After it is called once, the update method will be automatically called every delay milliseconds
        self.delay = 15
        self.update()

        self.window.mainloop()

    def get_custom_message(self, name):
        # normalize name
        name = " ".join(name.split("_"))
        if name in self.config['custom_messages']:
            return random.choice(self.config['custom_messages'][name])
        else:
            return "Hello " + name

    def say_msg(self, msg):
        print(msg)
        self.tts.say(msg)
        self.tts.runAndWait()

    def snapshot(self):
        # Get a frame from the video source
        ret, frame = self.vid.get_frame()

    def flip_face_recognizer(self):
        self.do_face_recognizer = not self.do_face_recognizer
        if self.do_face_recognizer:
            self.btn_face_recognization.config(relief="sunken")
        else:
            self.btn_face_recognization.config(relief="raised")

    def update_location(self, camera_id, name):
        try:
            resp = requests.post(self.config["tracker"]["host"] + "/add/" + str(self.config["camera_id"]),
                             params={"person_name": name})
            resp.raise_for_status()
            print("Updated location for " + name)
        except Exception as e:
            print("Error updating location for " + name)
        return

    def update(self):
        # Get a frame from the video source
        ret, frame = self.vid.get_frame()

        if (self.do_face_recognizer):
            # print("Finding Face and Drawing Box")
            face_locations, face_names = self.face_recog.analyze_frame(frame)
            frame = self.face_recog.draw_on_frame(frame, face_locations, face_names)

            fresh_names = []
            for name in face_names:
                if not self.last_seen_cache.has(name):
                    # Set counter for the number of frames the person has been seen
                    self.last_seen_cache.add(name, 0)

                if not self.greeting_cache.has(name):
                    fresh_names.append(name)
                    self.greeting_cache.add(name, True)
                self.greeting_cache.set(name, True)  # refresh cache

                # Increment counter if <= threshold. This conditional is just so we don't increment indefinitely.
                if self.last_seen_cache.get(name) <= self.config["tracker"]["frame_threshold"]:
                    self.last_seen_cache.set(name, self.last_seen_cache.get(name) + 1)

                # If the user has been detected for a certain number of frames within a certain amount of time
                # Update the db of the location
                if self.last_seen_cache.get(name) == self.config["tracker"]["frame_threshold"]:
                    self.update_location(1, name)
                self.last_seen_cache.set(name, self.last_seen_cache.get(name))  # refresh cache

            if len(fresh_names) > 0:
                # say custom message for every name
                [self.say_msg(self.get_custom_message(name)) for name in fresh_names]

        if ret:
            # print("Final Image")
            self.photo = PIL.ImageTk.PhotoImage(image = PIL.Image.fromarray(cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)))
            self.canvas.create_image(0, 0, image = self.photo, anchor = tkinter.NW)

        self.window.after(self.delay, self.update)

    def load_training_images(self, directory="profile_images/"):
        trained = TrainedData()

        for person_name in os.listdir(directory):
            for file in os.listdir(os.path.join(directory, person_name)):
                ext = os.path.splitext(file.lower())[1].lower()
                if ext in (".jpg", ".jpeg", ".png"):
                    filepath = os.path.join(directory, person_name, file)
                    print("Training with image: " + filepath)
                    try:
                        image = face_recognition.load_image_file(filepath)
                        encoding = face_recognition.face_encodings(image)[0]

                        trained.add(person_name, encoding)
                    except:
                        pass
        return trained

    def clear_cache(self):
        print("Clearing cache")
        self.greeting_cache.clear()
        self.last_seen_cache.clear()

class MyVideoCapture:
    def __init__(self, video_source=0):
        # Open the video source
        self.vid = cv2.VideoCapture(video_source)
        if not self.vid.isOpened():
            raise ValueError("Unable to open video source", video_source)

        # Get video source width and height
        self.width = self.vid.get(cv2.CAP_PROP_FRAME_WIDTH)
        self.height = self.vid.get(cv2.CAP_PROP_FRAME_HEIGHT)

    def get_frame(self):
        if self.vid.isOpened():
            ret, frame = self.vid.read()
            frame = cv2.flip( frame, 1 )
            if ret:
                # Return a boolean success flag and the current frame
                return (ret, frame)
            else:
                return (ret, None)
        else:
            return (ret, None)

    # Release the video source when the object is destroyed
    def __del__(self):
        if self.vid.isOpened():
            self.vid.release()


class TrainedData:
    def __init__(self):
        self._encodings = []
        self._names = []

    def add(self, name, encoding):
        self._names.append(name)
        self._encodings.append(encoding)

    def getClosestFace(self, face, default="Unknown"):
        if len(self._encodings) == 0:
            return default

        distances = face_recognition.face_distance(self._encodings, face)
        name = self._names[distances.argmin()] if len(distances) else "Unknown"
        return name


class FaceRecognizer:
    def __init__(self, trained_data):
        self.config = json.loads(open("config.json").read())
        self.FRAME_SKIP_CONSTANT = self.config['FRAME_SKIP_CONSTANT']
        self.trained = trained_data
        self.skip_frame_counter = 0

        self.face_locations = []
        self.face_encodings = []
        self.face_names = []

        # Used for resizing frame for faster face recognition processing
        self.scale_frame = self.config['scale_frame']

        self.font = cv2.FONT_HERSHEY_SIMPLEX

    def analyze_frame(self, frame):
        # Resize frame
        small_frame = cv2.resize(frame, (0, 0), fx=self.scale_frame, fy=self.scale_frame)
        # Convert the image from BGR color (which OpenCV uses) to RGB color (which face_recognition uses)
        rgb_small_frame = small_frame[:, :, ::-1]

        # Only process every other frame of video to save time
        if self.skip_frame_counter == 0:
            self.face_names = []
            self.face_locations = []

            # Find all the faces and face encodings in the current frame of video
            self.face_locations = face_recognition.face_locations(rgb_small_frame)
            self.face_encodings = face_recognition.face_encodings(rgb_small_frame, self.face_locations)

            for face_encoding in self.face_encodings:
                # See if the face is a match for the known face(s)
                name = self.trained.getClosestFace(face_encoding)

                self.face_names.append(" ".join(name.split("_")))

        self.skip_frame_counter = (self.skip_frame_counter + 1) % self.FRAME_SKIP_CONSTANT
        return self.face_locations, self.face_names

    def draw_on_frame(self, frame, face_locations, face_names):
        # Display the results
        for (top, right, bottom, left), name in zip(face_locations, face_names):
            # Scale back up face locations since the frame we detected in was scaled to 1/4 size
            top = int(top / self.scale_frame)
            right = int(right / self.scale_frame)
            bottom = int(bottom / self.scale_frame)
            left = int(left / self.scale_frame)

            # Draw a box around the face
            cv2.rectangle(frame, (left, top), (right, bottom), (50, 205, 154), 2)

            # Draw a label with a name below the face
            cv2.rectangle(frame, (left, bottom - 35), (right, bottom), (0, 0, 255), cv2.FILLED)

            cv2.putText(frame, name, (left + 6, bottom - 6), self.font, 1.0, (255, 255, 255), 1)

        # print("Drawn on Frame", face_locations)
        return frame

# Create a window and pass it to the Application object
App(tkinter.Tk(), "Team Dream Spiders App")
